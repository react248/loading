import { Component } from "react";

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
  }

  changeState = () => {
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 2000);
  };

  componentDidMount() {
    this.changeState();
  }

  render() {
    return <h1>{this.state.isLoading ? "Loading" : "Witaj"}</h1>;
  }
}
